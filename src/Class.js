class Promo {
    /** Avant tout, on déclare les différentes propriétés de la classe. 
     *  Elles ne sont pas toutes obligatoirement dans le constructeur
     */
    nbApprenants;
    typePromo;
    formateur;
    nomsApprenants= [];
    /**
     *  Dans le constructeur, on indique les paramètres qu'on veut
     *  remplir à chaque fois qu'on créé une instance.
     *  Par exemple, ici, on ne met pas nomsApprenants dans le constructeur parce qu'on les ajoute après.
     *  
     * @param {number} param1 le nombre d'apprenants 
     * @param {string} param2 le type de promo
     * @param {string} param3 le nom du formateur
     */
    constructor(param1, param2, param3){
        this.nbApprenants= param1;
        this.typePromo= param2;
        this.formateur=param3;
    }

    /** Ici, créé une méthode qui ajoute un apprenant dans le tableau
     *  Le paramètre de la méthode est: nom
     *  Il va nous servir à dire quel nom on veut ajouter au tableau,
     *  quand on appelle la méthode on mettra le nom qu'on veut entre les parenthèses
     */
    ajouterApprenant(nom){
        this.nomsApprenants.push(nom)
    }

    /**Une méthode qui console log tous les noms du tableau à la suite */
    logNom(){
        for (const nom of this.nomsApprenants) {
            console.log(nom)
        }
    }

    // toHTML(){
    //     let conteneur = document.createElement('div');
    //     for (const nom of this.nomsApprenants) {
    //         let paragraphe = document.createElement('p');
    //         paragraphe.textContent = nom;
    //         conteneur.appendChild(paragraphe);
    //     }
    //     document.body.appendChild(conteneur) 
    // }

    /** Une méthode qui créé un élément html (conteneur),
     *  qui créé un paragraphe pour chaque apprenant,
     *  qui le met dans le conteneur,
     *  et qui le renvoie une fois qu'il est remplis.
     * 
     *  Au dessus, la même methode sans le return. 
     *  Elle appendChild directement à la fin de la méthode,
     *  au lieu de retourner un élément html. 
     *  C'est un désavantage, avec le return on peut appendChild ou l'on veut */
    toHTML(){
        let conteneur = document.createElement('div');
        for (const nom of this.nomsApprenants) {
            let paragraphe = document.createElement('p');
            paragraphe.textContent = nom;
            conteneur.appendChild(paragraphe);
        }
        return conteneur 
    }
}

/**On créé une instance en donnant tous les paramètres */
let promo2 = new Promo(23,'dev','jean')

/**On ajoute des apprenants au tableau */
promo2.ajouterApprenant('connard')
promo2.ajouterApprenant('Gentil apprenant')
promo2.ajouterApprenant('daniela')

/**On console log le 1er apprenant du tableau */
console.log(promo2.nomsApprenants[0])

/** On appelle toHTML
 *  La 1ère fois (celle qui est commentée), on appelle juste la méthode car elle appendChild directement
 *  La 2ème fois, on appendChild l'appel de la méthode. Ca marche, parce que le return de la fonction est
 *  un élément html 
 *  En gros:
 *  promo2.toHTML() = conteneur
 */
// promo2.toHTML()
document.body.appendChild(promo2.toHTML())