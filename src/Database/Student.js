export class Student  {
    id;
    name;
    firstName;
    birthDate;

    constructor(id,name,firstName,birthDate){
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.birthDate = birthDate;
    }
}