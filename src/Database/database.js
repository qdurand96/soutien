import {createPool} from "mysql2/promise";
import { Student } from "./Student";

/**On créé une variable de connection, grace à la fonction createPool, qui existe dans sql */
const connection = createPool('mysql://simplon:1234@localhost:3306/first_db');

/**La même chose, un peu plus découpé */
// const connection = createPool({host:'localhost', user: 'simplon', password: '1234', database:'first_db'});

/**On fait une fonction async qui va servir à récupérer les données de la BDD, et les mettre dans un tableau */
export async function creerUnTableaudEtudiants(){
    
    /**On se sert de la connection qu'on a créé pour envoyer une requête sql
     * C'est la même chose que de taper SELECT * FROM student dans un terminal, seuf qu'on le fait en js
     * rows = les différentes entrées de la BDD, c'est un tableau d'objet.
     * Ici c'est un tableau de student, avec leur id, leur nom, ect
     * fields = les paramètres de notre BDD: une colonne pour l'id, une pour le name, ect. 
     * On ne va pas s'en servir
     */
    const [rows, fields] = await connection.execute('SELECT * FROM student');

    /**On créé un tableau d'étudiants vide, la fonction va servir à le remplir et nous le renvoyer */
    let tableauEtudiants = [];

    /**On fait une boucle sur row(qui est un tableau de student)  */
    for (const item of rows) {
        
        /**On créé une variable, qui est un nouveau student, 
         * dont les parametres seront les différentes parties de notre item: l'id, le name, ect */
        let etudiant = new Student(item.id, item.name, item.first_name, item.birth_date);
        
        /**On ajoute ce student au tableau */
        tableauEtudiants.push(etudiant);
    }
    /**A la fin de la boucle, pour chaque objet de la bdd,
     * on a créé une instance de student, et elles sont toutes dans le tableau */


    /**On oublie pas de return le tableau, sinon notre fonction sert à rien */
    return tableauEtudiants;
}

/**Cette PUTAIN de fonction qui fait que le console log marche */
async function consoleLog() {
    let data = await creerUnTableaudEtudiants()
    console.log(data)
}

consoleLog()
